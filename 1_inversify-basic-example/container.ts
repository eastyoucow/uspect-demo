import { Container } from "inversify";
import "reflect-metadata";
import { Katana, Nunchucks } from "./entities";
import { IWeapon } from "./interfaces";
import { TYPES } from "./types";

const container = new Container();

container.bind<IWeapon>(TYPES.Weapon).to(Nunchucks); // или container.bind<IWeapon>(TYPES.Weapon).to(Katana);

export { container };
