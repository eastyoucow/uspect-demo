import { injectable, inject } from "inversify";
import "reflect-metadata";
import { INinja, IWeapon } from "./interfaces";
import { TYPES } from "./types";

@injectable()
export class Katana implements IWeapon {
  public hit() {
    return "hit with katana";
  }
}

@injectable()
export class Nunchucks implements IWeapon {
  public hit() {
    return "hit with nunchuks!";
  }
}

@injectable()
export class Ninja implements INinja {
  weapon: IWeapon;

  // Alternativaely you can use property injection instead of constructor injection
  // @inject(TYPES.Weapon) weapon: IWeapon;

  public constructor(
    @inject(TYPES.Weapon) weapon: IWeapon // constructor injection
  ) {
    this.weapon = weapon;
  }
}
