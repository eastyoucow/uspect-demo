import { container } from "./container";
import { IWeapon } from "./interfaces";
import { TYPES } from "./types";

// для тестпа получаем реализацию из контейнера
const mike = container.get<IWeapon>(TYPES.Weapon);

console.log("\x1b[35m", mike.hit());
