export interface IWeapon {
    hit(): string;
}

export interface INinja {
    weapon: IWeapon;
}