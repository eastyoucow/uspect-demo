import 'reflect-metadata';
import {Container, interfaces, inject, injectable} from 'inversify';

import {LoggerToken} from './services/logger';
import {LoggerService} from './services/logger/logger.service';
import {ConfigToken, resolveConfig} from './services/config';

export type CommonService = 'logger';

export class IocContainer {
    readonly container: Container;

    constructor() {
        this.container = new Container();
    }

    get<T>(serviceIdentifier: interfaces.ServiceIdentifier<T>): T {
        return this.container.get<T>(serviceIdentifier);
    }

    registerConfig(envPrefix: string, configFolder?: string) {
        this.container.bind(ConfigToken).toConstantValue(resolveConfig(envPrefix, configFolder));
    }
    
    registerSingleton(service: any | any[], identifier?: any) {
        if (Array.isArray(service)) {
            for (const s of service) {
                this.container
                    .bind(s)
                    .to(s)
                    .inSingletonScope();
            }
        } else {
            this.container
                .bind(identifier || service)
                .to(service)
                .inSingletonScope();
        }
    }

    registerConstant(identifier: any, value: any) {
        this.container.bind(identifier).toConstantValue(value);
    }

    registerCommon(services: CommonService[]) {
        for (const serviceName of services) {
            switch (serviceName) {
                case 'logger':
                    this.container
                        .bind(LoggerToken)
                        .to(LoggerService)
                        .inSingletonScope();
                    break;
            }
        }
    }
}

export {Container, injectable, inject};
