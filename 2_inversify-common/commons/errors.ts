/**
 * Класс-обертка для обработанных ошибок, по умолчанию не логируется
 */
 export class ApiError extends Error {
    statusCode = 400;
    silent = true;
    meta: any = null;
    originalError: Error | null = null;
    publicData: any = null;
    code = '';
    title = '';

    constructor(err: string) {
        super();

        this.message = err;
        this.name = this.constructor.name;
    }

    jsonPublic(): Object {
        return {
            name: this.name,
            message: this.message,
            code: this.code,
            ...(this.title ? {title: this.title} : {}),
        };
    }
}

/**
 * ошибка обработана, но такого быть не должно или мы должны обратить на неё внимание (появится в логах)
 */
export class ApiCriticalError extends ApiError {
    silent = false;
}

/**
 * Класс-обертка для всех необработанных ошибок в коде
 */
export class ApiUnhandledError extends ApiError {
    statusCode = 500;
    silent = false;

    constructor(err: Error, meta: any = {}) {
        super(err.message);

        this.meta = {
            ...meta,
            stack: err.stack,
        };
        this.originalError = err;
    }

    jsonPublic(): Object {
        return {
            ...super.jsonPublic(),
            message: 'Произошла ошибка',
        };
    }

    static wrap(error: Error, meta?: any): ApiError {
        if (error instanceof ApiError) {
            if (meta) {
                error.meta = {
                    ...error.meta,
                    ...meta,
                };
            }

            return error;
        }

        return new ApiUnhandledError(error, meta);
    }
}

export class ApiMethodNotFoundError extends ApiError {
    constructor(method: string) {
        super(`Метод API не найден`);
        this.statusCode = 404;
        this.meta = {method};
    }
}


