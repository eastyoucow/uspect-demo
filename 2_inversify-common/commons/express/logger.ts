import * as express from 'express';

import {ILoggerService} from '../services/logger';

export function logRequest(logger: ILoggerService): express.Handler {
    return (request: express.Request, response: express.Response, next: express.NextFunction): void => {
        const {cookie, ...headers} = request.headers;

        logger.info(`Incoming ${request.method} ${request.baseUrl}${request.url}`, {
            headers,
        });

        next();
    };
}

export function logResponse(logger: ILoggerService): express.Handler {
    return (request: express.Request, response: express.Response, next: express.NextFunction): void => {
        const {send: originalSend, end: originalEnd} = response;
        let responseBody: any = null;

        response.send = function (body: any): express.Response {
            responseBody = body;

            return originalSend.apply(this, (arguments as unknown) as [body?: any]);
        };

        response.end = function (): void {
            logger.info(`Outgoing ${request.method} ${request.url} ${response.statusCode}`, {
                responseBody,
                statusCode: response.statusCode,
            });

            return originalEnd.apply(
                this,
                (arguments as unknown) as [chunk: any, encoding: BufferEncoding, cb?: (() => void) | undefined],
            );
        };

        next();
    };
}
