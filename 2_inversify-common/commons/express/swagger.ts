import {Express, Request, Response, NextFunction} from 'express';
import * as swaggerUi from 'swagger-ui-express';

export interface ISwaggerConfig {
    key: string;
    spec: any;
}

export const addSwaggerEndpoint = (app: Express, instances: ISwaggerConfig[]) => {
    const baseUrl = '/swagger/';

    app.use(baseUrl, swaggerUi.serve, (req: Request, res: Response, next: NextFunction) => {
        const item = req.originalUrl.substr(baseUrl.length);
        const instance = instances.find(i => i.key === item);

        if (!instance) {
            next();

            return;
        }

        res.send(swaggerUi.generateHTML(instance.spec));
    });
};
