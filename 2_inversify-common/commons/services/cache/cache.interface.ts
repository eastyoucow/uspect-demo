export type ICacheKeyValues = Record<string, any>;

export interface ICacheServiceSetOptions {
    expire?: number;
}

export interface ICacheService {
    getKey(key: string): Promise<any>;
    setKey(key: string, value: any, opts?: ICacheServiceSetOptions): void;
    clearKey(key: string): void;
    getKeys(keys: string[]): Promise<ICacheKeyValues>;
    setKeys(kvs: ICacheKeyValues, opts?: ICacheServiceSetOptions): void;
}

export interface IServiceWithCache {
    cache: ICacheService;
}
