import {ICacheServiceSetOptions, IServiceWithCache} from './cache.interface';
import {createHash} from 'crypto';

export function cached(key: string, expire?: number) {
    return (target: IServiceWithCache, propertyKey: string, descriptor: PropertyDescriptor) => {
        const methodOriginal = descriptor.value;
        const cacheOptions: ICacheServiceSetOptions = {};

        if (expire) {
            cacheOptions.expire = expire;
        }

        descriptor.value = async function() {
            const cacheService = (this as IServiceWithCache).cache;

            if (cacheService) {
                const cache = await cacheService.getKey(key);

                if (cache) {
                    return Promise.resolve(cache);
                }

                return methodOriginal.call(this).then((result: any) => {
                    cacheService.setKey(key, result, cacheOptions);

                    return result;
                });
            }

            return methodOriginal.call(this);
        };
    };
}

export function cachedWithParams(key: string, expire?: number) {
    return (target: IServiceWithCache, propertyKey: string, descriptor: PropertyDescriptor) => {
        const methodOriginal = descriptor.value;
        const cacheOptions: ICacheServiceSetOptions = {};

        if (expire) {
            cacheOptions.expire = expire;
        }

        descriptor.value = async function(...params: string[]) {
            const cacheService = (this as IServiceWithCache).cache;
            const paramsQuery = params.reduce((query, param) => query + param + ':', '');

            const hash = createHash('md5')
                .update(paramsQuery)
                .digest('hex');
            const keyWithHash = `${key}:${hash}`;

            if (cacheService) {
                const cache = await cacheService.getKey(keyWithHash);

                if (cache) {
                    return Promise.resolve(cache);
                }

                return methodOriginal.call(this, ...params).then((result: any) => {
                    cacheService.setKey(keyWithHash, result, cacheOptions);

                    return result;
                });
            }

            return methodOriginal.call(this, ...params);
        };
    };
}
