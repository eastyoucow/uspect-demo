import {merge, mergeWith, isPlainObject, isBoolean, isInteger, camelCase} from 'lodash';
import * as path from 'path';

export const resolveJsonConfig = (configPath?: string): {} => {
    if (!configPath) {
        configPath = path.resolve(process.cwd(), 'config');
    }

    const ENV = process.env['npm_config_api'] || 'dev';
    const config = Object.create(null);

    merge(config, require(`${configPath}/default.json`));

    try {
        merge(config, require(`${configPath}/${ENV}.json`));
    } catch (e) {}

    return config;
};

export const resolveEnvConfig = (envPrefix: string): {} => {
    const config = Object.create(null);

    const separator = '__';

    Object.keys(process.env)
        .filter(key => key.substr(0, envPrefix.length + separator.length) === envPrefix + separator)
        .forEach((key: string) => {
            const path: string[] = key.split(separator);

            path.shift();

            let obj = config;

            while (path.length) {
                const elem = camelCase(path.shift());

                if (path.length) {
                    if (!(elem in obj)) {
                        obj[elem] = {};
                    }

                    obj = obj[elem];
                } else if (typeof obj[elem] !== 'object') {
                    obj[elem] = process.env[key];
                }
            }
        });

    return config;
};

export const resolveConfig = (envPrefix: string, configPath?: string): {} => {
    const config = resolveJsonConfig(configPath);

    mergeWith(config, resolveEnvConfig(envPrefix), (configValue: any, envValue: any) => {
        if (isPlainObject(configValue)) {
            if (!isPlainObject(envValue)) {
                return configValue;
            }
        } else if (!isPlainObject(envValue)) {
            if (isBoolean(configValue)) {
                if (envValue === 'true') {
                    return true;
                } else if (envValue === 'false') {
                    return false;
                }
            } else if (isInteger(configValue)) {
                return parseInt(envValue, 10);
            }
        } else {
            return configValue;
        }
    });

    return config;
};
