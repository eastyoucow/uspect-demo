import { resolveConfig } from './config.resolvers';
import { ConfigToken } from './token';

export {ConfigToken, resolveConfig};
