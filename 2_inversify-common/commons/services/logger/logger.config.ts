export enum ConfigLogLevel {
    error = 'error',
    warn = 'warn',
    info = 'info',
    verbose = 'verbose',
    debug = 'debug',
    silly = 'silly',
}

export interface ILoggerConfig {
    logger: {
        console: {
            enabled: boolean;
            level: ConfigLogLevel;
        };

        file: {
            path: string;
            level: ConfigLogLevel;
        };

        options?: {
            [key: string]: string;
        };
    };
}
