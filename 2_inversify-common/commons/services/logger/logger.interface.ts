export type LogMethod = (message: string, ...meta: any[]) => void;

export interface ILoggerService {
    init: () => Promise<void>;

    debug: LogMethod;
    info: LogMethod;
    warn: LogMethod;
    error: LogMethod;
}
