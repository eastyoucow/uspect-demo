import {injectable, inject} from 'inversify';
import * as path from 'path';

import {Logger, LoggerInstance, TransportInstance, transports} from 'winston';

require('winston-daily-rotate-file');

import {padStart} from 'lodash';

import {ILoggerConfig, ConfigLogLevel} from './logger.config';
import {ILoggerService} from './logger.interface';
import {RequestContext, IOperationContext} from '../operationContext';
import { ConfigToken } from '../config';

interface ILogContext {
    trace_id: string | undefined;
    request_url: string | undefined;
    request_method: string | undefined;
    request_body: string | undefined;
    request_query: string | undefined;
}

export interface IFormatterOptions {
    level: string;
    message: string;
    meta: any;
}

const mkdirp = require('mkdirp');

const pad = (str: number, targetLength: number = 2): string => padStart(str.toString(), targetLength, '0');

const formatDate = (dt: Date): string =>
    `${dt.getUTCFullYear()}-${pad(dt.getUTCMonth() + 1)}-${pad(dt.getUTCDate())} ` +
    `${pad(dt.getUTCHours())}:${pad(dt.getUTCMinutes())}:${pad(dt.getUTCSeconds())}.${pad(dt.getUTCMilliseconds(), 3)}`;

const getLogContext = (defaults: IOperationContext = {}): ILogContext => {
    const operationContext = RequestContext.context;

    return {
        trace_id: defaults.requestId || operationContext.requestId || '',
        request_url: operationContext.requestUrl,
        request_method: operationContext.requestMethod,
        request_body: operationContext.requestBody,
        request_query: operationContext.requestQuery,
    };
};

const LOG_FILE_SIZE = 10485760;
const LOG_FILES_LIMIT = 10;

@injectable()
export class LoggerService implements ILoggerService {
    @inject(ConfigToken)
    protected config!: ILoggerConfig;

    protected logger!: LoggerInstance;

    async init(): Promise<void> {
        if (this.config.logger && !this.logger) {
            const loggerTransports: TransportInstance[] = [];

            const level = (lev: string) => (lev in ConfigLogLevel ? lev : ConfigLogLevel.info);

            if (this.config.logger.console && this.config.logger.console.enabled) {
                loggerTransports.push(
                    new transports.Console({
                        level: level(this.config.logger.console.level),
                        json: true,
                        stringify: obj => JSON.stringify(this.formatConsoleLog(obj)),
                    }),
                );
            }

            if (this.config.logger.file && this.config.logger.file.path && this.config.logger.file.path.length) {
                const fileName = path.basename(this.config.logger.file.path);
                const dirName = path.dirname(this.config.logger.file.path);

                try {
                    mkdirp.sync(dirName);
                } catch (e) {
                    console.error(e.toString());
                    throw e;
                }

                loggerTransports.push(
                    new transports.DailyRotateFile({
                        filename: `%DATE%.${fileName}`,
                        dirname: dirName,
                        maxsize: LOG_FILE_SIZE,
                        maxFiles: LOG_FILES_LIMIT,
                        prepend: true,
                        level: level(this.config.logger.file.level),
                        json: false,
                        formatter: (options: IFormatterOptions): string => JSON.stringify(this.formatFileLog(options)),
                    }),
                );
            }

            if (loggerTransports.length) {
                this.logger = new Logger({
                    transports: loggerTransports,
                });
            }
        }
    }

    protected formatFileLog(options: IFormatterOptions): any {
        const context = getLogContext(options.meta);

        return {
            ...(this.config.logger.options || {}),
            time: formatDate(new Date()),
            level: options.level,
            message: options.message,
            data: JSON.stringify(options.meta),
            ...context,
        };
    }

    protected formatConsoleLog(data: any): any {
        const context = getLogContext(data);

        return {
            time: formatDate(new Date()),
            level: data.level,
            message: data.message,
            data: JSON.stringify(data),
            ...context,
        };
    }

    debug(message: string, ...meta: any[]) {
        if (this.logger) {
            this.logger.debug(message, ...meta);
        }
    }

    info(message: string, ...meta: any[]) {
        if (this.logger) {
            this.logger.info(message, ...meta);
        }
    }

    warn(message: string, ...meta: any[]) {
        if (this.logger) {
            this.logger.warn(message, ...meta);
        }
    }

    error(message: string, ...meta: any[]) {
        if (this.logger) {
            this.logger.error(message, ...meta);
        }
    }

    alert(message: string, data: any = {}) {
        if (this.logger) {
            this.logger.warn(message, {
                ...(data || {}),
                requestLabel: 'alert',
            });
        }
    }
}
