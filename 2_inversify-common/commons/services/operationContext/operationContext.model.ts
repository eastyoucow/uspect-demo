export const REQUEST_CONTEXT = 'REQUEST_CONTEXT';
export const REQUEST_ID = 'REQUEST_ID';
export const REQUEST_USER = 'REQUEST_USER';
export const SERVICE_NAME = 'SERVICE_NAME';

export interface IOperationContext {
    requestId?: string;
    requestBody?: string;
    requestQuery?: string;
    requestUrl?: string;
    requestMethod?: string;
    requestIp?: string;
    requestHostname?: string;
}
