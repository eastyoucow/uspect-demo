import {AsyncLocalStorage} from 'async_hooks';
import * as express from 'express';
import * as uuid from 'uuid/v4';

import {LABEL_HEADER, TRACE_HEADER} from '../../constants';
import {REQUEST_CONTEXT, IOperationContext, REQUEST_ID, SERVICE_NAME} from './operationContext.model';

export class OperationContext {
    static readonly namespace = new AsyncLocalStorage<Map<string, unknown>>();

    static get<T>(key: string): T | null {
        const store = this.namespace.getStore();

        if (store) {
            return store.get(key) as T;
        }

        return null;
    }

    static set<T>(key: string, value: T): T | null {
        const store = this.namespace.getStore();

        if (store) {
            store.set(key, value);

            return value;
        }

        return null;
    }

    static update<T extends object>(key: string, value: T): T | null {
        const prev = this.get<T>(key);

        if (Array.isArray(value)) {
            return this.set<T>(key, ((prev || []) as any[]).concat(value) as T);
        }

        return this.set<T>(key, Object.assign(prev || {}, value));
    }
}

export class RequestContext {
    static get id(): string {
        return OperationContext.get(REQUEST_ID) || '';
    }

    static get context(): IOperationContext {
        return OperationContext.get(REQUEST_CONTEXT) || {};
    }
}

export function createContext(serviceName: string): express.RequestHandler {
    // tslint:disable-next-line only-arrow-functions
    return function(request: express.Request, response: express.Response, next: express.NextFunction) {
        const currentStore = new Map();

        currentStore.set(SERVICE_NAME, serviceName);

        OperationContext.namespace.run(currentStore, () => next());
    };
}

export function setContext(): express.RequestHandler {
    return function(request: express.Request, response: express.Response, next: express.NextFunction) {
        const traceId = request.get(TRACE_HEADER) || uuid();

        OperationContext.set<IOperationContext>(REQUEST_CONTEXT, {
            requestId: traceId,
            requestBody: JSON.stringify(request.body),
            requestQuery: JSON.stringify(request.query),
            requestUrl: request.url,
            requestMethod: request.method,
            requestIp: request.ip,
            requestHostname: request.hostname,
        });
        OperationContext.set(REQUEST_ID, traceId);

        response.setHeader(TRACE_HEADER, traceId);

        next();
    };
}
