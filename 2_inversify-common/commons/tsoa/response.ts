import {Response as ExpressResponse} from 'express';

export class TsoaResponse {
    static createHtmlResponse(html: string): TsoaResponse {
        const response = new TsoaResponse();

        response.data = html;

        return response;
    }

    static addResponseHandler(key: string, handler: (response: ExpressResponse, value: any) => void) {
        TsoaResponse.responseHandlers[key] = handler;
    }

    private static responseHandlers: Record<string, (response: ExpressResponse, value: any) => void> = {};

    status = 200;
    data = '';

    private handlerKey!: string;
    private handlerValue: any;

    set(key: string, value: any) {
        if (!TsoaResponse.responseHandlers[key]) {
            throw new Error('неизвестный формат ответа');
        }

        this.handlerKey = key;
        this.handlerValue = value;
    }

    send(response: ExpressResponse): any {
        if (this.handlerKey) {
            return TsoaResponse.responseHandlers[this.handlerKey](response, this.handlerValue);
        }

        response.status(this.status).send(this.data);
    }
}
