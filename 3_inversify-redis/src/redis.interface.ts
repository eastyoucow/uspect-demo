export interface IRedisConfig {
    redis: {
        host: string;
        port: number;
        prefix?: string;
    };
}
