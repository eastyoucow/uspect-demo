import {injectable, inject} from '@uspect/inversify-commons/dist/container';
import {ILoggerService, LoggerToken} from '@uspect/inversify-commons/dist/services/logger';
import {ConfigToken} from '@uspect/inversify-commons/dist/services/config';
import {
    ICacheService,
    ICacheKeyValues,
    ICacheServiceSetOptions,
} from '@uspect/inversify-commons/dist/services/cache';
import * as IORedis from 'ioredis';
import {RedisOptions} from 'ioredis';

import {IRedisConfig} from './redis.interface';

@injectable()
export class RedisService implements ICacheService {
    @inject(ConfigToken)
    private readonly config!: IRedisConfig;

    @inject(LoggerToken)
    private readonly logger!: ILoggerService;

    private redis!: IORedis.Redis;
    private redisHost!: string;
    private redisPort!: number;

    get host(): string {
        return this.redisHost;
    }

    get port(): number {
        return this.redisPort;
    }

    async init() {
        if (!this.config || !this.config.redis || !this.config.redis.host || !this.config.redis.port) {
            this.logger.info('[REDIS] connection is not configured');

            return;
        }

        let {host, port} = this.config.redis;

        const options: RedisOptions = {lazyConnect: true};

        if (host.includes(':')) {
            const t = host.split(':');

            host = t[0];
            port = parseInt(t[1], 10);
        }

        this.redisHost = host;
        this.redisPort = port;

        if (this.config.redis.prefix) {
            options.keyPrefix = this.config.redis.prefix.trim();
        }

        this.redis = new IORedis(port, host, options);
        await this.redis.connect();

        this.logger.info(`[REDIS] connection is started for ${host}:${port} prefix '${options.keyPrefix || ''}'`);
    }

    isActive(): boolean {
        return !!this.redis;
    }

    async getKey(key: string): Promise<any> {
        if (!this.redis) {
            return null;
        }

        const value = await this.redis.get(key);

        if (value === null) {
            return null;
        }

        try {
            return JSON.parse(value);
        } catch {
            return null;
        }
    }

    async getKeys(keys: string[]): Promise<ICacheKeyValues> {
        if (!this.redis) {
            return keys.map(() => null);
        }

        const results = await this.redis.pipeline(keys.map(key => ['get', key])).exec();
        const out: ICacheKeyValues = {};

        for (let i = 0; i < results.length; i++) {
            if (!results[i][0] && results[i][1]) {
                try {
                    out[keys[i]] = JSON.parse(results[i][1]);
                } catch (e) {
                    out[keys[i]] = null;
                }
            }
        }

        return out;
    }

    async clearKeysByPattern(pattern: string) {
        if (!this.redis) {
            return;
        }

        const keys = await this.getKeysByPattern(pattern);
        const keyPrefix = (this.redis.options && this.redis.options.keyPrefix) || '';
        const pipeline = this.redis.pipeline();

        for (const key of keys) {
            pipeline.del(key.replace(keyPrefix, ''));
        }

        pipeline.exec();
    }

    getKeysByPattern(pattern: string): Promise<string[]> {
        if (!this.redis) {
            return Promise.resolve([]);
        }

        return this.redis.keys(pattern);
    }

    setKey(key: string, value: any, opts?: ICacheServiceSetOptions) {
        if (!this.redis) {
            return;
        }

        const params: [string, string] = [key, JSON.stringify(value)];

        if (opts && opts.expire) {
            params.push('ex', `${opts.expire}`);
        }

        this.redis.set(...params);
    }

    setKeys(kvs: ICacheKeyValues, opts?: ICacheServiceSetOptions) {
        if (!this.redis) {
            return;
        }

        const params = [];

        for (const key of Object.keys(kvs)) {
            const param = ['set', key, JSON.stringify(kvs[key])];

            if (opts && opts.expire) {
                param.push('ex', `${opts.expire}`);
            }

            params.push(param);
        }

        this.redis.pipeline(params).exec();
    }

    clearKey(key: string) {
        if (!this.redis) {
            return;
        }

        this.redis.del(key);
    }
}
