import { JobStatus } from "bull";
import { IBaseTask } from "./queue.interface";

export abstract class BaseTask implements IBaseTask {
  abstract queue: string;
  abstract key: string;
}

export class STATUSES {
  static readonly ACTIVE: JobStatus[] = ["active"]; // задача в работе
  static readonly WAITING: JobStatus[] = ["waiting"]; // задача ждет в очереди
  static readonly DELAYED: JobStatus[] = ["waiting", "delayed"]; // задача ждёт очереди или отложена
  static readonly COMPLETED: JobStatus[] = ["completed"]; // задача завершена успешно
  static readonly FAILED: JobStatus[] = ["failed"]; // задача завершена с ошибкой
  static readonly QUEUED: JobStatus[] = ["waiting", "active", "delayed"]; // задача в очереди
}
