export { Queue } from "bull";
import { JobOptions } from "bull";

export interface IBaseTask {
  queue: string;
  key: string;
  name?: string;
}

export interface IQueueConfig {
  queue: {
    prefix: string;
  };
  redis: {
    host: string;
    port: number;
  };
}

export type QueueMap = Record<string, JobOptions>;

export interface IQueueProcessorOpts {
  jobId: string;
}

export interface IQueueIdenticalTasksOptions {
  [taskKey: string]: {
    // поля по которым мы определяем идентичность задач
    props: string[];
  };
}
