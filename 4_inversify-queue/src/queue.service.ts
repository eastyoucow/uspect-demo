import * as Bull from "bull";
import {
  JobOptions,
  Queue,
  CronRepeatOptions,
  EveryRepeatOptions,
  Job,
  JobStatus,
} from "bull";

import { injectable, inject } from "@uspect/inversify-commons/dist/container";
import {
  ILoggerService,
  LoggerToken,
} from "@uspect/inversify-commons/dist/services/logger";

import { ConfigToken } from "@uspect/inversify-commons/dist/services/config";

import {
  IBaseTask,
  IQueueConfig,
  IQueueIdenticalTasksOptions,
  QueueMap,
} from "./queue.interface";
import { STATUSES } from ".";

const wrapJobId = (queueId: string, queueJobId: string): string =>
  `${queueId}|${queueJobId}`;
const unwrapJobId = (jobId: string): [string, string] => {
  const t = jobId.split("|");

  return [t[0], t[1]];
};

@injectable()
export abstract class QueueService {
  @inject(ConfigToken)
  private readonly config!: IQueueConfig;

  @inject(LoggerToken)
  protected readonly logger!: ILoggerService;

  private queues: Record<string, Queue> = {};
  private isStarted = false;

  get started(): boolean {
    return this.isStarted;
  }

  async init() {
    if (!this.config.redis || !this.config.redis.host) {
      this.logger.warn("[QUEUE] Async queue is not configured");

      return;
    }

    let { host, port } = this.config.redis;

    if (host.includes(":")) {
      const t = host.split(":");

      host = t[0];
      port = parseInt(t[1], 10);
    }

    for (const queueId of this.getQueueIds()) {
      this.queues[queueId] = new Bull(queueId, {
        redis: { host, port },
        prefix: this.config.queue.prefix,
      });
      await this.queues[queueId].isReady();
    }

    this.logger.info(
      `[QUEUE] Async queue is started for redis '${host}:${port}'` +
        ` key prefix '${this.config.queue.prefix}'`
    );

    this.isStarted = true;
  }

  abstract getQueueIds(): string[];

  getQueuesDefaultOptions(): QueueMap {
    return {};
  }

  getIdenticalTasksOptions(): IQueueIdenticalTasksOptions {
    return {};
  }

  async addAsync(task: IBaseTask): Promise<string | null> {
    const job = await this.addInternal(task);

    return job ? wrapJobId(task.queue, job.id.toString()) : null;
  }

  add(task: IBaseTask) {
    this.addInternal(task).catch();
  }

  addRepeatable(
    task: IBaseTask,
    repeat: CronRepeatOptions | EveryRepeatOptions
  ) {
    this.addInternal(task, { repeat }).catch();
  }

  getQueue(queueId: string): Queue {
    return this.queues[queueId];
  }

  async addAndWaitResult<O = undefined>(
    task: IBaseTask,
    timeout?: number
  ): Promise<O | void> {
    const jobId = await this.addAsync(task);

    if (!jobId) {
      return;
    }

    return this.waitResult(jobId, timeout);
  }

  async waitResult(jobId: string, timeout?: number): Promise<any> {
    return new Promise((resolvePromise, rejectPromise) => {
      let endTimeout: NodeJS.Timeout;
      let isActive = true;
      const resolve = (value?: any) => {
        if (!isActive) {
          return;
        }

        isActive = false;

        if (endTimeout) {
          clearTimeout(endTimeout);
        }

        resolvePromise(value);
      };
      const reject = (value?: any) => {
        if (!isActive) {
          return;
        }

        isActive = false;

        if (endTimeout) {
          clearTimeout(endTimeout);
        }

        rejectPromise(value);
      };

      if (!jobId) {
        reject(new Error("job is missed"));

        return;
      }

      const [queueId, queueJobId] = unwrapJobId(jobId);
      const queue = this.queues[queueId];

      if (!queue) {
        reject(new Error("incorrect jobId passed"));
      }

      const jobInfo = (job: Job) =>
        `${job.id} (${job.data.queue} ${job.data.key})`;

      if (timeout) {
        endTimeout = setTimeout(() => {
          reject(new Error(jobId));
        }, timeout);
      }

      const resultChecker = () => {
        queue
          .getJob(queueJobId)
          .then((job) => {
            if (!job) {
              this.logger.info(`job is missed`);

              return resolve(null);
            }

            return job.getState().then((status) => {
              if (status === "completed") {
                this.logger.info(
                  `[QUEUE] Задача ${jobInfo(job)} успешно завершена`,
                  {
                    result: job.returnvalue,
                  }
                );

                resolve(job.returnvalue);
              } else if (status === "failed") {
                this.logger.info(`[QUEUE] Ошибка задачи ${jobInfo(job)}`);

                reject();
              } else if (isActive) {
                setTimeout(resultChecker, 500);
              }
            });
          })
          .catch((error) => {
            reject(error);
          });
      };

      resultChecker();
    });
  }

  protected async addInternal(
    task: IBaseTask,
    options?: JobOptions
  ): Promise<Job | null> {
    if (!this.isStarted) {
      this.logger.info("Очередь не инициализирована, задача не добавлена", {
        task,
      });

      return null;
    }

    const activeJob = await this.findIdenticalJob(task);

    if (activeJob) {
      this.logger.info("Подобная задача уже существует в очереди", { task });

      return activeJob;
    }

    const queueId = task.queue;

    if (!options) {
      options = this.getQueuesDefaultOptions()[queueId] || {};
    }

    if (options.removeOnComplete === undefined) {
      options.removeOnComplete = true;
    }

    if (!queueId) {
      this.logger.error(
        `У задачи ${task.constructor.name} не указан идентификатор очереди`
      );

      return null;
    }

    if (!(queueId in this.queues)) {
      this.logger.error(`Очередь ${queueId} не существует`);

      return null;
    }

    const queue = this.queues[queueId];

    try {
      const job = await queue.add(
        task.name || task.key || "default",
        task,
        options
      );

      this.logger.info(
        `[QUEUE] '${task.key}' добавлена в очередь '${queueId}' с id ${job.id}`,
        {
          jobId: job.id,
          queueId,
          data: job.data,
        }
      );

      return job;
    } catch (err) {
      this.logger.error(
        `[QUEUE] Ошибка добавления в очередь '${queueId}' задачи '${task.key}'`,
        {
          error: err,
          queueId,
          data: task,
        }
      );

      throw err;
    }
  }

  async clean(queueId: string) {
    const queue = this.getQueue(queueId);

    if (queue) {
      await queue.clean(0, "delayed");
      await queue.clean(0, "wait");
      await queue.clean(0, "active");
      await queue.clean(0, "completed");
      await queue.clean(0, "failed");
    }
  }

  protected async findIdenticalJob(task: IBaseTask): Promise<Job | null> {
    const options = this.getIdenticalTasksOptions()[task.key];

    if (!options) {
      return null;
    }

    const jobs = await this.findQueuedJobs(task.queue);

    for (const job of jobs) {
      if (
        job &&
        job.data &&
        job.data.key === task.key &&
        options.props.every(
          (prop) =>
            (task as any)[prop] && (task as any)[prop] === job.data[prop]
        )
      ) {
        return job;
      }
    }

    return null;
  }

  protected async findQueuedJobs(queueId: string): Promise<Job[]> {
    return this.findJobsByStatus(queueId, STATUSES.QUEUED);
  }

  protected async findJobsByStatus(
    queueId: string,
    status: JobStatus[]
  ): Promise<Job[]> {
    const queue = this.getQueue(queueId);

    if (!queue) {
      throw new Error("[QUEUE] очередь не существует");
    }

    return queue.getJobs(status);
  }

  protected async isJobActive(job: Job): Promise<boolean> {
    const status = await job.getState();

    return STATUSES.ACTIVE.includes(status);
  }
}
