import { Job, DoneCallback, Queue } from "bull";

import { injectable, inject } from "@uspect/inversify-commons/dist/container";
import {
  ILoggerService,
  LoggerToken,
} from "@uspect/inversify-commons/dist/services/logger";

import { IBaseTask, IQueueProcessorOpts } from "./queue.interface";

interface ILogErrorResult {
  message: string;
  stack?: string;
}

function logError(error: Error | unknown): ILogErrorResult | null {
  if (!error) {
    return null;
  }

  if (error instanceof Error) {
    return {
      message: error.message,
      stack: error.stack,
    };
  }

  return { message: error.toString() };
}

export interface IWorkerQueueService {
  getQueue: (queueId: string) => Queue;
}

@injectable()
export abstract class QueueWorker {
  @inject(LoggerToken)
  private readonly logger!: ILoggerService;

  protected abstract queueService: IWorkerQueueService;

  init() {
    this.initQueuesHandlers();
  }

  // передаем список очередей, которые будем обрабатывать
  abstract getQueueIds(): string[];

  // реализуем обработчики в приложении
  abstract taskProcessor(
    task: IBaseTask,
    opts?: IQueueProcessorOpts
  ): Promise<any>;

  private initQueuesHandlers() {
    for (const queueId of this.getQueueIds()) {
      const queue = this.queueService.getQueue(queueId);

      if (!queue) {
        continue;
      }

      queue.on("active", (job: Job<IBaseTask>) => {
        this.logger.info(`[QUEUE] job '${job.data.key}' started (${job.id})`, {
          jobId: job.id,
          data: job.data,
        });
      });

      queue.on("completed", (job: Job<IBaseTask>, result) => {
        this.logger.info(
          `[QUEUE] job '${job.data.key}' completed (${job.id})`,
          {
            jobId: job.id,
            queueId,
            result,
            data: job.data,
          }
        );
      });

      queue.on("error", (error) => {
        this.logger.error("[QUEUE] error", { error });
      });

      queue.on("failed", (job: Job<IBaseTask>, error) => {
        const jobJson = job.toJSON();
        const attempts =
          jobJson.opts && jobJson.opts.attempts ? jobJson.opts.attempts : 1;

        const logMessage =
          `[QUEUE] job '${job.data.key}' (${job.id}) failed ` +
          `${jobJson.attemptsMade} of ${attempts} tries`;
        const logArg = {
          jobId: job.id,
          queueId,
          data: job.data,
          error: logError(error),
        };

        this.logger.error(logMessage, logArg);
      });

      queue.process("*", (job: Job<IBaseTask>, jobDone: DoneCallback) => {
        const workerPromise = this.taskProcessor(job.data, {
          jobId: job.id.toString(),
        });

        if (!workerPromise) {
          const errorMessage = `[QUEUE] unsupported job type '${job.data.key}'`;

          this.logger.error(errorMessage, {
            jobId: job.id,
            queueId,
          });

          jobDone(new Error(errorMessage));

          return;
        }

        workerPromise
          .then((result) => {
            jobDone(null, result);
          })
          .catch((error) => {
            jobDone(error);
          });
      });
    }
  }
}
