# uspect-demo

**Проект с демки**

Полезные ссылки:

_DI_

https://habr.com/ru/company/tinkoff/blog/540662/

_SOLID_

https://habr.com/ru/company/productivity_inside/blog/505430/

https://medium.com/webbdev/solid-4ffc018077da

_inversify_

https://inversify.io/

https://pvictorsys.medium.com/dependency-injection-in-typescript-with-inversify-e956fa28b668
https://github.com/inversify/inversify-express-utils

https://medium.com/@debshish.pal/publish-a-npm-package-locally-for-testing-9a00015eb9fd

_tsoa_

https://github.com/lukeautry/tsoa
https://tsoa-community.github.io/docs/

_async_hooks_

https://stackabuse.com/using-async-hooks-for-request-context-handling-in-node-js/

bull

https://optimalbits.github.io/bull/
https://docs.bullmq.io/what-is-bullmq

rabbit

https://www.rabbitmq.com/tutorials/tutorial-one-javascript.html

express-sse

https://www.digitalocean.com/community/tutorials/nodejs-server-sent-events-build-realtime-app

redis (persistence)

https://redis.io/topics/persistence
