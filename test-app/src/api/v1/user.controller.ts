import { Route, Post, Body, Get, Query, Example } from "tsoa";
import { inject, injectable } from "../../ioc";
import { IUser } from "../../model.ts/user";
import { QueueService, TaskExample } from "../../services/queue";
import { UserService } from "../../services/user/user.service";

// @Security()
@Route("v1/user")
@injectable()
export class ApiV1UserController {
  @inject(UserService)
  private userService: UserService;

  @inject(QueueService)
  private queueService: QueueService;

  /**
   * @summary Получить пользователя
   */
  @Get("")
  async getUsers(): Promise<IUser[]> {
    return this.userService.getUsers();
  }

  /**
   * @summary Создать пользователя
   */
  @Get("test-queue/{id}")
  async testQueue(id: string): Promise<void> {
    return this.queueService.add(new TaskExample(id));
  }
}
