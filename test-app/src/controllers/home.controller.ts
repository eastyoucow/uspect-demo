import { controller, httpGet } from "@uspect/inversify-commons/dist/express";
import { homeHTML } from "@uspect/inversify-commons/dist/express/home";
import { inject } from "../ioc";
import { LoggerService, LoggerToken } from "../services/logger/logger.service";
import { TimeService } from "../services/time/time.service";

@controller("")
export class HomeController {
  @inject(LoggerToken)
  logger: LoggerService;

  @inject(TimeService)
  timeService: TimeService;

  @httpGet("/")
  public get(): string {
    return homeHTML;
  }

  @httpGet("/time")
  public getTime(): Promise<string> {
    return this.timeService.getTime();
  }

  @httpGet("/time-cached")
  public getTimeCached(): Promise<string> {
    return this.timeService.getTimeCached();
  }
}
