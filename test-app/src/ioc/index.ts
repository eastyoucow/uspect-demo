import 'reflect-metadata';

import {IocContainer} from '@uspect/inversify-commons/dist/container';
import {inject, injectable} from '@uspect/inversify-commons/dist/container';

const iocContainer = new IocContainer();

export {iocContainer, IocContainer, inject, injectable};
