import { LoggerService, LoggerToken } from "../services/logger/logger.service";
import { iocContainer } from "./";
import "../controllers/home.controller";
import { TimeService } from "../services/time/time.service";
import { RedisService } from "@uspect/inversify-redis/dist";

// испортируем tsoa контроллеры
import { controllers } from "../api";
import { UserService } from "../services/user/user.service";
import { QueueService } from "../services/queue";
import { AppWorkerService } from "../services/queue-workers";

// iocContainer.registerCommon(['logger']); // use common logger
iocContainer.registerSingleton(LoggerService, LoggerToken); // extend common logger
iocContainer.registerSingleton([
  RedisService,
  TimeService,
  UserService,
  QueueService,
  AppWorkerService,
]);

iocContainer.registerConfig("USPECT");
iocContainer.registerSingleton(controllers);
