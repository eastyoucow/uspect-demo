export interface IUser {
    name: string;
    age: number;
    isNinja: boolean;
}