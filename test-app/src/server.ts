import {iocContainer} from './ioc';
import './ioc/loader';

import {ILoggerService, LoggerToken} from '@uspect/inversify-commons/dist/services/logger';
import {AppService} from './services/app/app.service';

iocContainer.registerSingleton([AppService]);

(<AppService>iocContainer.get(AppService))
    .init()
    .then(() => {})
    .catch(error => {
        (<ILoggerService>iocContainer.get(LoggerToken)).error('Ошибка при инициализации приложения', {error});
    });
