import { InversifyExpressServer } from "@uspect/inversify-commons/dist/express";
import {
  LoggerToken,
  ILoggerService,
} from "@uspect/inversify-commons/dist/services/logger";
import {
  createContext,
  setContext,
} from "@uspect/inversify-commons/dist/services/operationContext";
import { addSwaggerEndpoint } from "@uspect/inversify-commons/dist/express/swagger";
import {
  logRequest,
  logResponse,
} from "@uspect/inversify-commons/dist/express/logger";
import { inject, injectable, iocContainer } from "../../ioc";
import { RedisService } from "@uspect/inversify-redis/dist";

import * as express from "express";
import * as bodyParser from "body-parser";
import { ConfigToken, IConfig } from "../config/config.interface";

// tsoa swagger
import swagger = require("../../api/swagger.json");
//tsoa routes
import { RegisterRoutes } from "../../api/routes";
import { QueueService, TaskExample } from "../queue";

import * as Arena from "bull-arena";
import { Queues } from "../queue/queue.const";
import { Queue } from "@uspect/inversify-queue/dist";

const PORT = 4000;

@injectable()
export class AppService {
  @inject(LoggerToken)
  private logger: ILoggerService;

  @inject(ConfigToken)
  private config: IConfig;

  @inject(RedisService)
  private redis: RedisService;

  @inject(QueueService)
  private queueServices: QueueService;

  private app: express.Application;

  async init() {
    try {
      const { port } = await this.initInternal();

      this.logger.info("Server running on port", port);
    } catch (error) {
      this.logger.error("Can't start application", error);

      process.exit(1);
    }
  }

  private async initInternal(): Promise<{ port: number }> {
    const logger = <ILoggerService>iocContainer.get(LoggerToken);
    const redis = <RedisService>iocContainer.get(RedisService);
    const queue = <QueueService>iocContainer.get(QueueService);
    const server = new InversifyExpressServer(iocContainer.container, null, {
      rootPath: "/api",
    });

    await logger.init();
    await redis.init();
    await queue.init();

    await this.queueServices.clean(Queues.ExampleQueue);

    this.queueServices.addRepeatable(new TaskExample("test"), {
      cron: "* * * * *",
    });

    await server.setConfig((app: express.Express) => {
      const apiPrefix = ["api"];

      addSwaggerEndpoint(app, [{ key: "internal", spec: swagger }]);
      app.use(bodyParser.json());
      app.use(apiPrefix, createContext("uspect api"));
      app.use(apiPrefix, setContext());
      app.use(apiPrefix, logRequest(this.logger));
      app.use(apiPrefix, logResponse(this.logger));

      const queues: any = [];

      [...Object.values(Queues)].forEach((queueName) => {
        queues.push({
          name: queueName,
          hostId: "uspect",
          prefix: this.config.queue.prefix,
          redis: { host: this.redis.host, port: this.redis.port },
        });
      });

      app.use(
        "/",
        Arena(
          {
            queues,
          },
          {
            basePath: "/arena",
            disableListen: true,
          }
        )
      );

      RegisterRoutes(app);
    });

    this.app = <any>server.build();
    this.app.listen(this.config.port);

    return {
      port: this.config.port,
    };
  }
}
