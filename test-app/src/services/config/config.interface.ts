export { ConfigToken } from "@uspect/inversify-commons/dist/services/config";

export interface IConfig {
  port: number;
  portWorkers: number;

  queue: {
    prefix: string;
  };
}
