import {injectable} from '../../ioc';

import {LoggerService as BaseLoggerService} from '@uspect/inversify-commons/dist/services/logger/logger.service';
import {LoggerToken} from '@uspect/inversify-commons/dist/services/logger/logger.token';

@injectable()
export class LoggerService extends BaseLoggerService {
    alertBusiness(message: string, data?: any) {
        this.warn(message, {
            ...(data || {}),
            requestLabel: 'business',
        });
    }
}

export {LoggerToken};
