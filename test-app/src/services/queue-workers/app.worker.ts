import {
  LoggerToken,
  ILoggerService,
} from "@uspect/inversify-commons/dist/services/logger";
import { homeHTML } from "@uspect/inversify-commons/dist/express/home";
import * as express from "express";
import { inject, injectable, iocContainer } from "../../ioc";

import { IConfig, ConfigToken } from "../config/config.interface";
import { QueueWorker } from "./queue.worker";
import { QueueService } from "../queue/queue.service";
import { RedisService } from "@uspect/inversify-redis/dist";

interface IInitResult {
  port: number;
}

@injectable()
export class AppWorkerService {
  @inject(ConfigToken)
  private config: IConfig;

  @inject(LoggerToken)
  private logger: ILoggerService;

  @inject(QueueWorker)
  private queueWorker: QueueWorker;

  async init() {
    try {
      const { port } = await this.initInternal();

      this.logger.info("Worker running on port", port);
    } catch (error) {
      this.logger.error("Can't start application", error);

      process.exit(1);
    }
  }

  private async initInternal(): Promise<IInitResult> {
    const queueService = <QueueService>iocContainer.get(QueueService);
    const redis = <RedisService>iocContainer.get(RedisService);
    const logger = <ILoggerService>iocContainer.get(LoggerToken);

    await queueService.init();
    await redis.init();
    await logger.init();

    this.queueWorker.init();

    const app = express();
    const port = this.config.portWorkers;

    app.use("/hc", (req: express.Request, res: express.Response) => {
      res.send(homeHTML);
    });

    app.listen(port);

    return { port };
  }
}
