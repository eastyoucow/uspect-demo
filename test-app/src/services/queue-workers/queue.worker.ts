import {
  QueueWorker as BaseQueueWorker,
  IBaseTask,
} from "@uspect/inversify-queue/dist";

import { inject, injectable } from "../../ioc";
import { QueueService, Queues, Jobs } from "../queue";
import { ConfigToken, IConfig } from "../config/config.interface";
import { TimeService } from "../time/time.service";

@injectable()
export class QueueWorker extends BaseQueueWorker {
  @inject(QueueService)
  protected queueService: QueueService;

  @inject(ConfigToken)
  protected readonly config: IConfig;

  @inject(TimeService)
  timeService: TimeService;

  getQueueIds(): string[] {
    return [Queues.ExampleQueue];
  }

  taskProcessor(task: IBaseTask): Promise<any> {
    switch (task.key) {
      case Jobs.ExampleJob:
        return this.timeService.longTaskExample();
    }
  }
}
