export enum Queues {
  ExampleQueue = "example",
}

export enum Jobs {
  ExampleJob = "example_job",
}
