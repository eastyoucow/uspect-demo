import { inject, injectable } from "../../ioc";

import {
  QueueService as BaseQueueService,
  QueueMap,
  IQueueIdenticalTasksOptions,
} from "@uspect/inversify-queue/dist";
import { Queues, Jobs } from "./queue.const";
import { ConfigToken, IConfig } from "../config/config.interface";

@injectable()
export class QueueService extends BaseQueueService {
  getQueueIds(): string[] {
    return [Queues.ExampleQueue];
  }

  getQueuesDefaultOptions(): QueueMap {
    return {
      [Queues.ExampleQueue]: {
        // attempts: 5,
        // backoff: { type: "exponential", delay: 5000 },
      },
    };
  }

  getIdenticalTasksOptions(): IQueueIdenticalTasksOptions {
    return {
      [Jobs.ExampleJob]: { props: ["entityId"] },
    };
  }
}
