import { Jobs, Queues } from ".";

export class TaskExample {
  queue = Queues.ExampleQueue;
  key = Jobs.ExampleJob;
  name: string;

  constructor(public entityId: string) {}
}
