import { inject, injectable } from "../../ioc";
import { cachedWithParams } from "@uspect/inversify-commons/dist/cache";
import { RedisService } from "@uspect/inversify-redis/dist";

@injectable()
export class TimeService {
  @inject(RedisService)
  private redisService: RedisService;

  get cache() {
    return this.redisService;
  }

  getTime() {
    return Promise.resolve(new Date().toLocaleTimeString());
  }

  @cachedWithParams("time:cache", 10)
  getTimeCached() {
    return Promise.resolve(new Date().toLocaleTimeString());
  }

  async longTaskExample() {
    await new Promise((resolve) => setTimeout(resolve, 6000));
  }
}
