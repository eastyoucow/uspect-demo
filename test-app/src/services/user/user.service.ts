import { injectable } from "../../ioc";
import { IUser } from "../../model.ts/user";



@injectable()
export class UserService {
    users: IUser[] = [];

    getUsers(): Promise<IUser[]> {
        return Promise.resolve(this.users);
    }

    postUser(user: IUser): Promise<IUser[]> {
        this.users.push(user);

        return Promise.resolve(this.users);
    }
}