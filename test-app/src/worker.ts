import { iocContainer } from "./ioc";
import "./ioc/loader";

import {
  ILoggerService,
  LoggerToken,
} from "@uspect/inversify-commons/dist/services/logger";

import { AppWorkerService, QueueWorker } from "./services/queue-workers";

iocContainer.registerSingleton([QueueWorker]);
(<AppWorkerService>iocContainer.get(AppWorkerService))
  .init()
  .then(() => {})
  .catch((error) => {
    (<ILoggerService>iocContainer.get(LoggerToken)).error(
      "Ошибка при инициализации воркеров",
      { error }
    );
  });
